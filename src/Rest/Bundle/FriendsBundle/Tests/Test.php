<?php

namespace Rest\Bundle\FriendsBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class Test
 * @package Rest\Bundle\FriendsBundle\Tests
 */
class Test extends WebTestCase
{

    /**
     * @var
     */
    protected $container;

    /**
     *
     */
    public function test()
    {
        static::bootKernel();
        $this->container = static::$kernel->getContainer();

        $em = $this->container->get('neo4j.manager');

        $em->createCypherQuery()
            ->query("MATCH (n)-[r]-() DELETE n, r")
            ->getResult();


        $em->createCypherQuery()
            ->query("MATCH (n) DELETE n")
            ->getResult();

        $userRepository = $this->container->get('rest_friends.user_repository');

        for($i=1; $i<=20; $i++){
            $userRepository->createUser('test'.$i);
        }

        $this->startTest();

    }
}