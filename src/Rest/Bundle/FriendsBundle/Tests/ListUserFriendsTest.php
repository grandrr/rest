<?php

namespace Rest\Bundle\FriendsBundle\Tests;

use FOS\RestBundle\Util\Codes;

/**
 * Class ListUserFriendsTest
 * @package Rest\Bundle\FriendsBundle\Tests
 */
class ListUserFriendsTest extends Test
{

    public function startTest()
    {
        $container = static::$kernel->getContainer();

        $userRepository = $container->get('rest_friends.user_repository');

        $user1 = $userRepository->findOneBy(array('fullName' => 'test1'));
        $user2 = $userRepository->findOneBy(array('fullName' => 'test5'));

        $requestUser1 = $userRepository->findOneBy(array('fullName' => 'test2'));
        $requestUser2 = $userRepository->findOneBy(array('fullName' => 'test3'));
        $requestUser3 = $userRepository->findOneBy(array('fullName' => 'test4'));
        $requestUser4 = $userRepository->findOneBy(array('fullName' => 'test10'));
        $requestUser5 = $userRepository->findOneBy(array('fullName' => 'test11'));
        $requestUser6 = $userRepository->findOneBy(array('fullName' => 'test12'));
        $requestUser7 = $userRepository->findOneBy(array('fullName' => 'test13'));

        $client = static::createClient();


        //create friendships

        $client->request('POST', '/rest/create_friendship/', array(
                'user' => $user1->getId(),
                'request_user' => $requestUser1->getId(),
            )
        );

        $statusCode = $client->getResponse()->getStatusCode();
        $this->assertEquals($statusCode, Codes::HTTP_CREATED);


        $client->request('POST', '/rest/create_friendship/', array(
                'user' => $user1->getId(),
                'request_user' => $requestUser2->getId(),
            )
        );

        $statusCode = $client->getResponse()->getStatusCode();
        $this->assertEquals($statusCode, Codes::HTTP_CREATED);

        $client->request('POST', '/rest/create_friendship/', array(
                'user' => $user1->getId(),
                'request_user' => $requestUser3->getId(),
            )
        );

        $statusCode = $client->getResponse()->getStatusCode();
        $this->assertEquals($statusCode, Codes::HTTP_CREATED);

        $client->request('POST', '/rest/create_friendship/', array(
                'user' => $user2->getId(),
                'request_user' => $requestUser3->getId(),
            )
        );

        $statusCode = $client->getResponse()->getStatusCode();
        $this->assertEquals($statusCode, Codes::HTTP_CREATED);

        $client->request('POST', '/rest/create_friendship/', array(
                'user' => $user1->getId(),
                'request_user' => $requestUser4->getId(),
            )
        );

        $statusCode = $client->getResponse()->getStatusCode();
        $this->assertEquals($statusCode, Codes::HTTP_CREATED);

        $client->request('POST', '/rest/create_friendship/', array(
                'user' => $requestUser4->getId(),
                'request_user' => $requestUser5->getId(),
            )
        );

        $statusCode = $client->getResponse()->getStatusCode();
        $this->assertEquals($statusCode, Codes::HTTP_CREATED);

        $client->request('POST', '/rest/create_friendship/', array(
                'user' => $requestUser5->getId(),
                'request_user' => $requestUser6->getId(),
            )
        );

        $statusCode = $client->getResponse()->getStatusCode();
        $this->assertEquals($statusCode, Codes::HTTP_CREATED);

        $client->request('POST', '/rest/create_friendship/', array(
                'user' => $requestUser6->getId(),
                'request_user' => $requestUser7->getId(),
            )
        );

        $statusCode = $client->getResponse()->getStatusCode();
        $this->assertEquals($statusCode, Codes::HTTP_CREATED);


        $client->request('GET', '/rest/list_friendships/' . $requestUser3->getId());

        $responseContent = $client->getResponse()->getContent();
        $statusCode = $client->getResponse()->getStatusCode();

        $this->assertEquals($statusCode, Codes::HTTP_FOUND);

        $jsonContent = json_decode($responseContent, true);

        $this->assertEquals(count($jsonContent['friendships']), 2);


        //add user to friends from friendship

        foreach ($jsonContent['friendships'] as $user) {

            $client->request('POST', '/rest/add_friend/',
                array(
                    'user' => $requestUser3->getId(),
                    'friend' => $user['user']['id']
                )
            );

            $statusCode = $client->getResponse()->getStatusCode();

            $this->assertEquals($statusCode, Codes::HTTP_OK);

        }


        $this->addFriends($client, $requestUser7->getId());


        //get list friends

        $client->request('GET', "/rest/get_friends/{$requestUser7->getId()}/3");

        $statusCode = $client->getResponse()->getStatusCode();
        $response = $client->getResponse()->getContent();

        $this->assertEquals($statusCode, Codes::HTTP_FOUND);

        $friendsData = json_decode($response, true);

        $this->assertEquals(count($friendsData['result']), 3);

    }

    /**
     * @param $client
     * @param $userId
     */
    protected function addFriends($client, $userId)
    {
        $client->request('GET', '/rest/list_friendships/' . $userId);

        $responseContent = $client->getResponse()->getContent();
        $statusCode = $client->getResponse()->getStatusCode();

        $this->assertEquals($statusCode, Codes::HTTP_FOUND);

        $jsonContent = json_decode($responseContent, true);


        foreach ($jsonContent['friendships'] as $userFriendship) {

            $client->request('POST', '/rest/add_friend/',
                array(
                    'user' => $userId,
                    'friend' => $userFriendship['user']['id']
                )
            );

            $statusCode = $client->getResponse()->getStatusCode();

            $this->assertEquals($statusCode, Codes::HTTP_OK);

            $this->addFriends($client, $userFriendship['user']['id']);

        }

    }

}