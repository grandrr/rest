<?php

namespace Rest\Bundle\FriendsBundle\Tests;

use FOS\RestBundle\Util\Codes;

/**
 * Class CreateFriendshipRequestTest
 * @package Rest\Bundle\FriendsBundle\Tests
 */
class CreateFriendshipRequestTest extends Test
{

    public function startTest()
    {
        $container = static::$kernel->getContainer();

        $userRepository = $container->get('rest_friends.user_repository');

        $user1 = $userRepository->findOneBy(array('fullName' => 'test1'));
        $user2 = $userRepository->findOneBy(array('fullName' => 'test5'));

        $requestUser1 = $userRepository->findOneBy(array('fullName' => 'test2'));
        $requestUser2 = $userRepository->findOneBy(array('fullName' => 'test3'));
        $requestUser3 = $userRepository->findOneBy(array('fullName' => 'test4'));

        $client = static::createClient();


        //create friendships

        $client->request('POST', '/rest/create_friendship/', array(
                'user' => $user1->getId(),
                'request_user' => $requestUser1->getId(),
            )
        );

        $statusCode = $client->getResponse()->getStatusCode();
        $this->assertEquals($statusCode, Codes::HTTP_CREATED);


        $client->request('POST', '/rest/create_friendship/', array(
                'user' => $user1->getId(),
                'request_user' => $requestUser2->getId(),
            )
        );

        $statusCode = $client->getResponse()->getStatusCode();
        $this->assertEquals($statusCode, Codes::HTTP_CREATED);

        $client->request('POST', '/rest/create_friendship/', array(
                'user' => $user1->getId(),
                'request_user' => $requestUser3->getId(),
            )
        );

        $statusCode = $client->getResponse()->getStatusCode();
        $this->assertEquals($statusCode, Codes::HTTP_CREATED);

        $client->request('POST', '/rest/create_friendship/', array(
                'user' => $user2->getId(),
                'request_user' => $requestUser3->getId(),
            )
        );

        $statusCode = $client->getResponse()->getStatusCode();
        $this->assertEquals($statusCode, Codes::HTTP_CREATED);


        //get list friendships

        $client->request('GET', '/rest/list_friendships/' . $requestUser3->getId());

        $responseContent = $client->getResponse()->getContent();
        $statusCode = $client->getResponse()->getStatusCode();

        $this->assertEquals($statusCode, Codes::HTTP_FOUND);

        $jsonContent = json_decode($responseContent, true);

        $this->assertEquals(count($jsonContent['friendships']), 2);

    }
}