<?php

namespace Rest\Bundle\FriendsBundle\Entity\Repository;

use HireVoice\Neo4j\Repository;
use Rest\Bundle\FriendsBundle\Entity\Friendship;
use Rest\Bundle\FriendsBundle\Entity\User;

/**
 * Class UserRepository
 * @package Rest\Bundle\FriendsBundle\Repository
 */
class UserRepository extends Repository
{
    /**
     * @param $name
     * @return bool
     */
    public function createUser($name)
    {
        $em = $this->getEntityManager();

        $user = new User();
        $user->setFullName($name);

        $em->persist($user);
        $em->flush();

        return true;
    }

    /**
     * @param User $user
     * @param User $friend
     * @return bool
     */
    public function addFriendToUser(User $user, User $friend)
    {
        $em = $this->getEntityManager();

        $user->addFriend($friend);

        $em->persist($user);
        $em->flush();

        return true;
    }

    /**
     * @param $user
     * @param $depth
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getUserFriends(User $user, $depth = 1)
    {
        $em = $this->getEntityManager();

        return $em->createCypherQuery()
            ->query("MATCH (user {fullName: '".$user->getFullName()."'})-[:friend*1..".$depth."]->(userFriends) RETURN userFriends")

//            ->startWithNode('user', $user)
//            ->match('(user { fullName:"'.$user->getFullName().'" })-[r*0..'.$depth.']-(follow)')
//            ->end('DISTINCT user', 'follow')

//            ->startWithNode('user', $user)
//            ->match('(user { fullName:"'.$user->getFullName().'" })-[:friend*0..'.$depth.']-(userFriends)')
//            ->end('userFriends')

            ->getResult();
    }

    /**
     * @param User $user
     * @param User $friendship
     * @return bool
     */
    public function addFriendshipToUser(User $user, User $friendship)
    {
        $em = $this->getEntityManager();

        $user->addFriendShip($friendship);

        $em->persist($user);
        $em->flush();

        return true;
    }


    /**
     * @param User $user
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getFriendshipsByUser(User $user)
    {
        $em = $this->getEntityManager();

        return $em->createCypherQuery()
            ->query("MATCH (user)-[:friendShip]->(friendship {fullName:'" . $user->getFullName() . "'})
            RETURN user")
            ->getResult();
    }

    /**
     * @param User $user
     * @param User $friendship
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function removeFriendshipFromUser(User $user, User $friendship)
    {
        $em = $this->getEntityManager();

        return $em->createCypherQuery()
            ->query("MATCH (userStart {fullName:'" . $user->getFullName() . "'})-[rel:friendShip]
            ->(friendship {fullName:'" . $friendship->getFullName() . "'}) DELETE rel")
            ->getResult();
    }

    /**
     * @param User $user
     * @param User $friendship
     */
    public function acceptFriendshipFromUser(User $user, User $friendship)
    {
        $this->addFriendToUser($user, $friendship);

        $this->removeFriendshipFromUser($friendship, $user);
    }
}
