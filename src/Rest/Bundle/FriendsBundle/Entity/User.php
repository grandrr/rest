<?php

namespace Rest\Bundle\FriendsBundle\Entity;

use HireVoice\Neo4j\Annotation as OGM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * All entity classes must be declared as such.
 *
 * @OGM\Entity(repositoryClass="Rest\Bundle\FriendsBundle\Entity\Repository\UserRepository")
 */
class User
{
    /**
     * The internal node ID from Neo4j must be stored. Thus an Auto field is required
     * @OGM\Auto
     */
    protected $id;

    /**
     * @OGM\Property
     * @OGM\Index
     */
    protected $fullName;

    /**
     * @OGM\ManyToMany
     */
    protected $friends;

    /**
     * @OGM\ManyToMany
     */
    protected $friendShip;

//    /**
//     * @OGM\ManyToOne
//     */
//    protected $referrer;
//
//    /**
//     * @var ArrayCollection
//     */
//    protected $friends;

//    /**
//     *
//     */
//    function __construct()
//    {
//        $this->friends = new ArrayCollection;
//    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @param $fullName
     * @return $this
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFriends()
    {
        return $this->friends;
    }

    /**
     * @param $user
     * @return $this
     */
    public function addFriend($user)
    {
        $this->friends[] = $user;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFriendShip()
    {
        return $this->friendShip;
    }

    /**
     * @param mixed $friendShip
     */
    public function addFriendShip($friendShip)
    {
        $this->friendShip[] = $friendShip;
    }

}
