<?php

namespace Rest\Bundle\FriendsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('RestFriendsBundle:Default:index.html.twig', array('name' => $name));
    }
}
