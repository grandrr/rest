<?php

namespace Rest\Bundle\FriendsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Util\Codes;

class RestController extends FOSRestController
{

//    public function listUsersAction()
//    {
//        $userRepository = $this->get('rest_friends.user_repository');
//
//        $users = $userRepository->findAll();
//
//        $view = $this->view()
//            ->setData(array('users' => $users))
//            ->setStatusCode(Codes::HTTP_FOUND)
//            ->setFormat('json')
//        ;
//
//        return $this->handleView($view);
//    }

    public function createUserAction(Request $request)
    {
        $name = $request->get('name');

        $userRepository = $this->get('rest_friends.user_repository');

        $result = $userRepository->createUser($name);

        if($result){
            $view = $this->view()
                ->setData(array('result' => $result))
                ->setStatusCode(Codes::HTTP_CREATED)
                ->setFormat('json')
            ;
        }else{
            $view = $this->view()
//                ->setData(array('result' => 'true'))
                ->setStatusCode(Codes::HTTP_OK)
//                ->setTemplate("RestFriendsBundle:Rest:createUser.html.twig")
//                ->setFormat('json')
            ;
        }

        return $this->handleView($view);

    }

    public function listFriendsAction(Request $request)
    {
        $userId = $request->get('user');
        $depth = $request->get('depth');

        $userRepository = $this->get('rest_friends.user_repository');

        if ($user = $userRepository->find($userId)) {

            $result = $userRepository->getUserFriends($user, $depth);

            $view = $this->view()
                ->setData(array('result' => $result, 'count'=>count($result)))
                ->setStatusCode(Codes::HTTP_FOUND)
                ->setFormat('json');

        } else {
            $view = $this->view()
                ->setData(array('result' => array()))
                ->setStatusCode(Codes::HTTP_OK)
                ->setFormat('json');
        }

        return $this->handleView($view);
    }

    public function createFriendshipAction(Request $request)
    {
        $userId = $request->get('user');
        $requestUserId = $request->get('request_user');

        $userRepository = $this->get('rest_friends.user_repository');
        $user = $userRepository->find($userId);
        $requestUser = $userRepository->find($requestUserId);

        if($user && $requestUser){

            $userRepository->addFriendshipToUser($user, $requestUser);

            $view = $this->view()
//                ->setData(array('result' => 'true'))
                ->setStatusCode(Codes::HTTP_CREATED)
                ->setFormat('json')
            ;
        }else{
            $view = $this->view()
                ->setStatusCode(Codes::HTTP_OK)
                ->setFormat('json')
            ;
        }

        return $this->handleView($view);

    }

    public function removeFriendshipAction(Request $request)
    {
        $userId = $request->get('user');
        $requestUserId = $request->get('request_user');

        $userRepository = $this->get('rest_friends.user_repository');
        $user = $userRepository->find($userId);
        $requestUser = $userRepository->find($requestUserId);

        if($user && $requestUser){

            $userRepository->removeFriendshipFromUser($requestUser, $user);

            $view = $this->view()
                ->setData(array('result' => 'true'))
                ->setStatusCode(Codes::HTTP_OK)
                ->setFormat('json')
            ;
        }else{
            $view = $this->view()
                ->setStatusCode(Codes::HTTP_OK)
                ->setFormat('json')
            ;
        }

        return $this->handleView($view);
    }

    public function listFriendshipsAction(Request $request)
    {
        $userId = $request->get('user');

        $userRepository = $this->get('rest_friends.user_repository');

        $user = $userRepository->find($userId);

//        $friendshipRepository = $this->get('rest_friends.friendship_repository');

        $friendships = $userRepository->getFriendshipsByUser($user);

        $view = $this->view()
            ->setData(array('friendships' => $friendships))
            ->setStatusCode(Codes::HTTP_FOUND)
            ->setFormat('json')
        ;

        return $this->handleView($view);
    }

    public function addFriendAction(Request $request)
    {
        $userId = $request->get('user');
        $friendId = $request->get('friend');

        $userRepository = $this->get('rest_friends.user_repository');

        $user = $userRepository->find($userId);
        $friend = $userRepository->find($friendId);

//        $userRepository->addFriendToUser($user, $friend);
        $userRepository->acceptFriendshipFromUser($user, $friend);

        $view = $this->view()
            ->setData(array('result' => 'true'))
            ->setStatusCode(Codes::HTTP_OK)
            ->setFormat('json')
        ;

        return $this->handleView($view);
    }

}
